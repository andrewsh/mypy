mypy (0.720-3) unstable; urgency=medium

  * debhelper-compat 12
  * Standards-Version: 4.4.0
  * Set upstream metadata fields: Name.
  * Pluck patch from upstream to cope with changes to lxml. Closes: #939045

 -- Michael R. Crusoe <michael.crusoe@gmail.com>  Mon, 02 Sep 2019 17:10:54 +0900

mypy (0.720-2) unstable; urgency=medium

  * Fix AutoPkgTest path

 -- Michael R. Crusoe <michael.crusoe@gmail.com>  Tue, 30 Jul 2019 19:47:02 +0200

mypy (0.720-1) unstable; urgency=medium

  * New upstream release. Closes: #930928.

 -- Michael R. Crusoe <michael.crusoe@gmail.com>  Sat, 13 Jul 2019 12:09:09 +0200

mypy (0.670-2) unstable; urgency=medium

  * Include upstream's README.md in the the mypy binary package.
  * Fix dependecy version between mypy and python3-mypy. Closes: #921990.

 -- Michael R. Crusoe <michael.crusoe@gmail.com>  Sun, 10 Feb 2019 00:25:00 -0800

mypy (0.670-1) unstable; urgency=medium

  * New upstream version
  * debian/control: bump and tighten the python3-typed-ast dependency
  * debian/clean: a few more directories of post build cruft

 -- Michael R. Crusoe <michael.crusoe@gmail.com>  Sun, 10 Feb 2019 00:19:11 -0800

mypy (0.660-5) unstable; urgency=medium

  * Reporting needs python3-lxml.

 -- Michael R. Crusoe <michael.crusoe@gmail.com>  Fri, 25 Jan 2019 10:13:54 -0800

mypy (0.660-4) unstable; urgency=medium

  * Fix the autopkgtests. Closes: #920048

 -- Michael R. Crusoe <michael.crusoe@gmail.com>  Thu, 24 Jan 2019 14:56:34 -0800

mypy (0.660-3) unstable; urgency=medium

  * Team upload.
  * Fix spelling of dependency
    Closes: #920325
  * debhelper 12

 -- Andreas Tille <tille@debian.org>  Thu, 24 Jan 2019 09:55:27 +0100

mypy (0.660-2) unstable; urgency=medium

  * Make explicit the dependencies of python3-mypy; add "Breaks" for older
    cwltool releases.

 -- Michael R. Crusoe <michael.crusoe@gmail.com>  Tue, 22 Jan 2019 15:53:22 -0800

mypy (0.660-1) unstable; urgency=medium

  * New upstream version
  * Standards-Version: 4.3.0, no changes needed
  * Ignore changes to ./*egg-info via debian/source/options
  * Run the provided tests during build and as an autopkgtest
  * python3-mypy-extensions is now from a separate source package
  * Support the nocheck and/or nodoc Build Profiles

 -- Michael R. Crusoe <michael.crusoe@gmail.com>  Sat, 19 Jan 2019 06:41:40 -0800

mypy (0.650-1) unstable; urgency=medium

  * New upstream version

 -- Michael R. Crusoe <michael.crusoe@gmail.com>  Mon, 10 Dec 2018 20:28:20 -0800

mypy (0.641-1) unstable; urgency=medium

  * New upstream release.

 -- Michael R. Crusoe <michael.crusoe@gmail.com>  Tue, 16 Oct 2018 07:19:06 -0700

mypy (0.630-2) unstable; urgency=medium

  * Fix the version of mypy_extension. Closes: #909070, #909068

 -- Michael R. Crusoe <michael.crusoe@gmail.com>  Tue, 18 Sep 2018 11:38:12 -0700

mypy (0.630-1) unstable; urgency=medium

  * New upstream release.
  * Include mypy_extensions.

 -- Michael R. Crusoe <michael.crusoe@gmail.com>  Mon, 17 Sep 2018 11:05:13 -0700

mypy (0.620-1) unstable; urgency=medium

  * New upstream release.

 -- Michael R. Crusoe <michael.crusoe@gmail.com>  Tue, 07 Aug 2018 11:09:09 -0700

mypy (0.610-1) unstable; urgency=medium

  * New upstream release.

 -- Michael R. Crusoe <michael.crusoe@gmail.com>  Mon, 11 Jun 2018 02:48:28 -0700

mypy (0.600-1) unstable; urgency=medium

  * New upstream release.
  * Add manpages for dmypy subcommands, as they are now officially in beta

 -- Michael R. Crusoe <michael.crusoe@gmail.com>  Sun, 06 May 2018 02:51:30 -0700

mypy (0.590-1) unstable; urgency=medium

  * Add basic autopkgtest and enable autopkgtest-pkg-python
  * Latest upstream.
  * Bump standards-version and update VCS fields to salsa

 -- Michael R. Crusoe <michael.crusoe@gmail.com>  Wed, 18 Apr 2018 03:57:17 -0700

mypy (0.580-2) unstable; urgency=medium

  * Team upload
  * Depends: python3-pkg-resources
    Closes: #894075

 -- Andreas Tille <tille@debian.org>  Wed, 04 Apr 2018 13:41:34 +0200

mypy (0.580-1) unstable; urgency=medium

  * Team upload.
  * New upstream version
  * debhelper 11
  * Add missing Depends: python3-distutils
    Closes: #894075
  * Docs moved to /usr/share/doc/mypy

 -- Andreas Tille <tille@debian.org>  Mon, 26 Mar 2018 13:20:59 +0200

mypy (0.570-1) unstable; urgency=medium

  * New upstream version.
  * Added manual page for dmypy

 -- Michael R. Crusoe <michael.crusoe@gmail.com>  Sat, 10 Mar 2018 08:55:23 -0800

mypy (0.560-1) unstable; urgency=medium

  * Team upload.
  * New upstream version
    Closes: #881604
  * Standards-Version: 4.1.2
  * New versioned Build-Depends: python3-psutil (>= 5.4.0)

 -- Andreas Tille <tille@debian.org>  Fri, 22 Dec 2017 09:16:43 +0100

mypy (0.540-2) unstable; urgency=medium

  * Team upload

  [ Ghislain Antony Vaillant ]
  * Run cme fix on control file
    - Sort Build-Depends
    - Wrap and sort Depends
  * Run cme fix on copyright file
    - Use Expat instead of MIT
  * Move source package to utils section
  * Normalize the DH_VERBOSE preamble
  * Provide the documentation in mypy-doc
  * Provide the modules in python3-mypy (Closes: #879195)
  * Update copyright information.
    Thanks to Thorsten Alteholz for the review

 -- Ghislain Antony Vaillant <ghisvail@gmail.com>  Tue, 28 Nov 2017 20:16:43 +0000

mypy (0.540-1) unstable; urgency=medium

  * New upstream release

 -- Michael R. Crusoe <michael.crusoe@gmail.com>  Mon, 23 Oct 2017 10:40:39 -0700

mypy (0.530-1) unstable; urgency=medium

  * New upstream release

 -- Michael R. Crusoe <michael.crusoe@gmail.com>  Sat, 07 Oct 2017 10:20:18 -0700

mypy (0.521-1) unstable; urgency=medium

  * New upstream release

 -- Michael R. Crusoe <michael.crusoe@gmail.com>  Thu, 27 Jul 2017 10:10:00 -0700

mypy (0.520-1) unstable; urgency=medium

  * New upstream release

 -- Michael R. Crusoe <michael.crusoe@gmail.com>  Fri, 14 Jul 2017 07:34:50 -0700

mypy (0.511-2) unstable; urgency=high

  * Merge fix from Ubuntu / Michael Hudson-Doyle to prevent ftbfs when multiple
    python 3 versions are supported. (Closes: #865946)
  * Patch one of upstream's tests to use python3 explicitly

 -- Michael R. Crusoe <michael.crusoe@gmail.com>  Tue, 11 Jul 2017 01:08:22 -0700

mypy (0.511-1ubuntu1) artful; urgency=medium

  * d/rules: fix manpage build when multiple versions of Python 3 are
    supported.

 -- Michael Hudson-Doyle <michael.hudson@ubuntu.com>  Mon, 26 Jun 2017 12:02:52 +1200

mypy (0.511-1) unstable; urgency=medium

  * Run the upstream tests
  * New upstream release
  * typed-ast is now a requirement, bump version req'd

 -- Michael R. Crusoe <michael.crusoe@gmail.com>  Sat, 24 Jun 2017 09:49:52 -0700

mypy (0.470-complete-1) unstable; urgency=medium

  * Use complete upstream source from PyPI, not incomplete tarball from
    GitHub. (Closes: #852819)

 -- Michael R. Crusoe <michael.crusoe@gmail.com>  Wed, 01 Feb 2017 02:59:28 -0800

mypy (0.470-1) unstable; urgency=medium

  * new upstream location
  * New upstream version 0.470

 -- Michael R. Crusoe <michael.crusoe@gmail.com>  Sun, 15 Jan 2017 01:28:37 -0800

mypy (0.4.6-1) unstable; urgency=medium

  * Team upload.
  * New upstream version
  * debhelper 10
  * d/watch: version=4

 -- Andreas Tille <tille@debian.org>  Thu, 01 Dec 2016 14:14:08 +0100

mypy (0.4.5-1) unstable; urgency=medium

  * recommend python3-typed-ast for the fast parser
  * new upstream-release

 -- Michael R. Crusoe <michael.crusoe@gmail.com>  Sat, 08 Oct 2016 02:12:32 -0700

mypy (0.4.4-1) unstable; urgency=medium

  * new upstream release

 -- Michael R. Crusoe <michael.crusoe@gmail.com>  Fri, 26 Aug 2016 00:52:42 -0700

mypy (0.4.3-1) unstable; urgency=medium

  * New upstream release
  * patch typeshed/tests/pytype_test.py to use python3

 -- Michael R. Crusoe <michael.crusoe@gmail.com>  Tue, 02 Aug 2016 07:34:58 -0700

mypy (0.4.2-2) unstable; urgency=medium

  * Update debian/copyrigh URL as excuse to rebuild package

 -- Michael R. Crusoe <michael.crusoe@gmail.com>  Wed, 29 Jun 2016 08:35:10 -0700

mypy (0.4.2-1) unstable; urgency=medium

  * New upstream release

 -- Michael R. Crusoe <michael.crusoe@gmail.com>  Thu, 09 Jun 2016 14:14:11 -0700

mypy (0.4.1-1) unstable; urgency=medium

  * New upstream release

 -- Michael R. Crusoe <michael.crusoe@gmail.com>  Tue, 10 May 2016 08:08:14 -0700

mypy (0.4-1) unstable; urgency=medium

  * New upstream release

 -- Michael R. Crusoe <michael.crusoe@gmail.com>  Fri, 06 May 2016 05:12:05 -0700

mypy (0.3.1-1) unstable; urgency=medium

  * Initial release. (Closes: #823512)

 -- Michael R. Crusoe <michael.crusoe@gmail.com>  Thu, 05 May 2016 06:00:25 -0700
